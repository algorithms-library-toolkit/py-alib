import unittest
import alib


class TestEnvironment(unittest.TestCase):
    def setUp(self):
        self.aql = alib.CLI({"key1": "val1", "key2": "val2"})

    def test_base(self):
        """Tests simple automaton query"""
        res = self.aql.query('print string::Parse @automaton::Automaton \"DFA a b\n>0 1 0\n<1 1 1\" | string::Compose -')
        self.assertEqual(res.result, res.CommandResult.EOT)
        self.assertEqual(res.stdout, "DFA a b\n>0 1 0\n<1 1 1")
        self.assertEqual(res.stderr, None)

    def test_bindings(self):
        """Tests bindings loaded"""
        res = self.aql.query("print #key1")
        self.assertEqual(res.result, res.CommandResult.EOT)
        self.assertEqual(res.stdout, "val1")
        self.assertEqual(res.stderr, None)
        self.assertEqual(res.stdlog, None)

    def test_environment(self):
        """Tests whether the environment is not reset after a call (persist a variable)"""
        res = self.aql.query("execute \"a\" > $var")
        self.assertEqual(res.result, res.CommandResult.EOT)
        self.assertIsNone(res.stdout)
        self.assertIsNone(res.stderr)
        self.assertIsNone(res.stdlog)

        res = self.aql.query("print $var")
        self.assertEqual(res.result, res.CommandResult.EOT)
        self.assertEqual(res.stdout, "a")
        self.assertIsNone(res.stderr)
        self.assertIsNone(res.stdlog)

    def test_debug_stream(self):
        """tests whether debug streams is correctly in stderr"""
        res = self.aql.query("set verbose 1; execute string::Parse @regexp::RegExp \"a\" | regexp::convert::ToAutomatonGlushkov - ")
        self.assertEqual(res.result, res.CommandResult.EOT)
        self.assertIsNone(res.stdout, None)
        self.assertIsNone(res.stderr, None)
        self.assertIsNotNone(res.stdlog)
        self.assertIn("First", res.stdlog)

        res = self.aql.query("set verbose 0; execute string::Parse @regexp::RegExp \"a\" | regexp::convert::ToAutomatonGlushkov - ")
        self.assertIsNone(res.stdlog)

    def test_raises(self):
        """cli raises exception"""
        with self.assertRaises(Exception):
            self.aql.query("execute string::Parse  @templateParam \"a\"")

    def test_raises_details(self):
        """cli raises correctly nested exceptions"""
        try:
            res = self.aql.query("execute string::Parse  @templateParam \"a\"")
            self.fail()
        except alib.AqlError as e:
            causes = []
            exc = e

            self.assertIsInstance(exc, alib.AqlError)
            causes.append(str(exc))

            while exc.__cause__ is not None:
                self.assertIsInstance(exc.__cause__, alib.AqlError)
                causes.append(str(exc.__cause__))

                exc = exc.__cause__

            causes = list(reversed(causes))
            self.assertEqual(causes,
                             ['[Standard exception]: Templated entry string::Parse < [templateParam] > not available',
                              '[Standard exception]: Evaluation of algorithm string::Parse failed.'])
