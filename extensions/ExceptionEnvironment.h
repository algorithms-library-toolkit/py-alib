#ifndef EXCEPTION_ENVIRONMENT_H_
#define EXCEPTION_ENVIRONMENT_H_

#include <environment/Environment.h>

/**
  * Overrides cli::Environment with own execute_line method which does not catch exceptions
  */
class ExceptionEnvironment : public cli::Environment {
public:
 	cli::CommandResult execute ( std::shared_ptr < cli::LineInterface > lineInterface );
 	cli::CommandResult execute_line ( cli::CharSequence charSequence );
};

#endif /* EXCEPTION_ENVIRONMENT_H_ */