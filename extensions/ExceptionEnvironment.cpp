#include "ExceptionEnvironment.h"
#include <parser/Parser.h>
#include <iostream>

cli::CommandResult ExceptionEnvironment::execute ( std::shared_ptr < cli::LineInterface > lineInterface ) {
	cli::CommandResult state = cli::CommandResult::OK;

	while ( state == cli::CommandResult::OK || state == cli::CommandResult::ERROR || state == cli::CommandResult::EXCEPTION )
    	state = execute_line ( cli::CharSequence ( lineInterface ) );

	return state;
}

cli::CommandResult ExceptionEnvironment::execute_line ( cli::CharSequence charSequence ) {
	cli::Parser parser = cli::Parser ( cli::Lexer ( std::move ( charSequence ) ) );
	cli::CommandResult res = parser.parse ( )->run ( * this );

	if ( res == cli::CommandResult::CONTINUE || res == cli::CommandResult::BREAK )
		throw std::logic_error ( "There is no loop to continue/break." );

	return res;
}