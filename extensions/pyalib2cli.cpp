#include <Python.h>

#include <exception/CommonException.h>
#include <global/GlobalData.h>
#include <alib/exception>

#include <readline/StringLineInterface.h>

#include "ExceptionEnvironment.h"

PyObject* AqlErrorType;

// ---------------------------------------------------------------------------------------------------------------------

typedef struct {
    PyObject_HEAD
    ExceptionEnvironment* environment; // This C-API somehow does not work with instance but OK with pointers. Maybe some C-style casts?
} CliEnvironmentObject;

static PyObject *
Aql_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    CliEnvironmentObject *self;
    self = (CliEnvironmentObject *) type->tp_alloc(type, 0);
    if (self != nullptr) {
    	self -> environment = nullptr; // should we allocate here or in init?
    }
    return (PyObject *) self;
}

static void
Aql_dealloc(CliEnvironmentObject *self) {
    PyTypeObject *tp = Py_TYPE(self);
    if (self->environment)
    	delete self -> environment;
    tp->tp_free(self);
}

static int
Aql_init(CliEnvironmentObject *self, PyObject *args, PyObject *kwds)
{
    static char *kwlist[] = { (char*)"bindings", nullptr };

    if ( self -> environment == nullptr )
    	self -> environment = new ExceptionEnvironment ( );

    PyObject *envDict; // bindings dictionary

    if ( ! PyArg_ParseTupleAndKeywords ( args, kwds, "|O!", kwlist, &PyDict_Type, &envDict ) ) {
		return -1;
	}

	PyObject *key, *val;
	const char *k, *v;
	Py_ssize_t pos = 0;
	while (PyDict_Next(envDict, &pos, &key, &val)) {
		if ( ! PyUnicode_CheckExact ( key ) || ! PyUnicode_CheckExact ( val ) ||
		     ! ( k = PyUnicode_AsUTF8 ( key ) ) || ! ( v = PyUnicode_AsUTF8 ( val ) ) ) {
			return -1;
		}

		self -> environment -> setBinding ( std::string ( k ), std::string ( v ) );
	}

    return 0;
}

static PyObject*
Aql_run ( CliEnvironmentObject * self, PyObject * args ) {
	char * input; /* code to execute, 1st argument */

	// parse arguments
	if ( ! PyArg_ParseTuple ( args, "s", &input ) ) {
		return NULL;
	}

	std::ostringstream oss_out, oss_err, oss_log;

	common::Streams::out = oss_out;
	common::Streams::err = oss_err;
	common::Streams::log = oss_log;

	cli::CommandResult state;
	try {
		state = self -> environment -> execute ( std::make_shared < cli::StringLineInterface > ( std::string ( input ) ) );
    } catch ( const std::exception & ) {
		alib::ExceptionHandler::NestedExceptionContainer exceptions;
		alib::ExceptionHandler::handle ( exceptions );

		PyObject *prev_exc, *type, *traceback, *curr_exc;

		auto it = exceptions.getExceptions ( ).rbegin ( );

		PyErr_SetString ( AqlErrorType, std::string ( *it ).c_str ( ) );
		PyErr_Fetch ( &type, &curr_exc, &traceback );
		PyErr_NormalizeException ( &type, &curr_exc, &traceback );

		for ( it = std::next ( it ); it != exceptions.getExceptions ( ).rend ( ); it ++ ) {
			prev_exc = curr_exc;
			PyErr_SetString ( AqlErrorType, std::string ( *it ).c_str ( ) );
			PyErr_Fetch ( &type, &curr_exc, &traceback );
			PyErr_NormalizeException ( &type, &curr_exc, &traceback );
			PyException_SetCause ( curr_exc, prev_exc );
		}

		PyErr_Restore ( type, curr_exc, traceback );

		return nullptr; // when throwing an exception, null must be returned
	}

	return Py_BuildValue ( "isss", static_cast < int > ( state ), oss_out.str().c_str(), oss_err.str().c_str(), oss_log.str().c_str());
}

// ---------------------------------------------------------------------------------------------------------------------

static PyMethodDef Aql_methods[] = {
    {"run", (PyCFunction) Aql_run, METH_VARARGS, "Run a query in current environment" },
    {NULL, NULL, 0, NULL}  /* Sentinel */
};

PyTypeObject CliEnvironmentType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "pyalib2cli.environment",
    .tp_basicsize = sizeof(CliEnvironmentType),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) Aql_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_doc = "alib2cli environment",
	.tp_methods = Aql_methods,
	.tp_init = (initproc) Aql_init,
    .tp_new = Aql_new,
};

// ---------------------------------------------------------------------------------------------------------------------

static PyMethodDef module_methods[ ] = {
	{ NULL, NULL, 0, NULL }
};

static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    "pyalib2cli", /* name of module */
    NULL,         /* module documentation, may be NULL */
    -1,           /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    module_methods
};

PyMODINIT_FUNC
PyInit_pyalib2cli(void) {
    PyObject *m;
    if (PyType_Ready(&CliEnvironmentType) < 0)
        return NULL;

    m = PyModule_Create(&module);
    if (m == NULL)
        return NULL;

    Py_INCREF(&CliEnvironmentType);
    if (PyModule_AddObject(m, "environment", (PyObject *) &CliEnvironmentType) < 0) {
        Py_DECREF(&CliEnvironmentType);
        Py_DECREF(m);
        return NULL;
    }

    AqlErrorType = PyErr_NewException("pyalib2cli.AltError", NULL, NULL);
    Py_INCREF(AqlErrorType);
    if (PyModule_AddObject(m, "AqlError", AqlErrorType) < 0) {
		Py_DECREF(&CliEnvironmentType);
		Py_DECREF(AqlErrorType);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}
