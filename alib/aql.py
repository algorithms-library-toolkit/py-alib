import enum

import pyalib2cli
import enum
from .utils import truncate as trunc


AqlError = pyalib2cli.AqlError


class CLI:
    class QueryResult:
        class CommandResult(enum.Enum):
            OK = 0
            QUIT = 1
            RETURN = 2
            CONTINUE = 3
            BREAK = 4
            EXCEPTION = 5
            ERROR = 6
            EOT = 7

        def __init__(self, result, stdout, stderr, stdlog):
            self.result = self.CommandResult(result)
            self.stdout = stdout.strip() if stdout.strip() != "" else None
            self.stderr = stderr.strip() if stderr.strip() != "" else None
            self.stdlog = stdlog.strip() if stdlog.strip() != "" else None

        def __repr__(self):
            return "(AqlResult {}, stdout={}, stderr={}, log={})".format(self.result,
                                                                         trunc(self.stdout),
                                                                         trunc(self.stdout),
                                                                         trunc(self.stdlog))

    def __init__(self, environment=None):
        if environment is None:
            environment = {}
        self.environment = pyalib2cli.environment(bindings=environment)

    def query(self, q):
        if not isinstance(q, str):
            raise TypeError("Query must be a string")

        result, stdout, stderr, stdlog = self.environment.run(q)  # can throw
        return self.QueryResult(result, stdout, stderr, stdlog)
