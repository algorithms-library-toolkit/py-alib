def truncate(string, max_len=10):
    if string is None:
        string = ""

    if len(string) > max_len:
        string = string[:max_len] + '...'

    string = string.replace("\r", "\\r").replace("\n", "\\n")

    return "\"{}\"".format(string)