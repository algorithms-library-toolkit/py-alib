from setuptools import find_packages, setup, Extension


EXTRA_LIBRARIES = ['alib2data', 'alib2algo', 'alib2str', 'alib2str_cli_integration', 'alib2raw', 'alib2raw_cli_integration']

pyalib2cli = Extension(
    "pyalib2cli",
    sources=["extensions/pyalib2cli.cpp", "extensions/ExceptionEnvironment.cpp"],
    extra_compile_args=['-std=c++17'],
    extra_link_args=['-Wl,-no-as-needed'] + list(map(lambda s: "-l{}".format(s), EXTRA_LIBRARIES)),
    include_dirs=['/usr/include/algorithms-library'],
    libraries=[
        'alib2cli',
        'alib2xml',
        'alib2common',
        'alib2abstraction',
        'alib2measure',
        'alib2std',
    ]
)

setup(name="pyalib",
      version="0.0.0",
      description="Python bindings for Algorithms Library Toolkit",
      long_description="",
      license="GPL3",
      author="Tomáš Pecka",
      author_email="peckato1@fit.cvut.cz",
      url="https://gitlab.fit.cvut.cz/algorithms-library-toolkit/",
      classifiers=[],

      test_suite="tests",
      ext_modules=[pyalib2cli],
      packages=["alib"],
      )
